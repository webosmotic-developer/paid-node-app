const TestUser = require('../schema/paid');
const express = require('express');
const jwt = require('jsonwebtoken');

const router = express.Router();

router.get('/LoginUser', (req, res) => {
  const email = req.query.email;
  const password = req.query.password;

  TestUser.find({email: email, password: password}).then((data) => {
    if (data.length !== 0) {
      const tokenData = ({email: email, password: password});
      const token = getToken(tokenData);
      res.status(200).json({'response': data, 'token': token});
    } else {
      res.status(403).json({'message': 'Invalid login credentials, Please try again!'});
    }
  }).catch((err) => {
    if (err) res.status(500).json({'message': 'Internal server error'});
  });
});

router.post('/SignUpUser', (req, res) => {
  const user = new TestUser(req.body);
  emailExistValidation(req.body.email).then((existEmail) => {
    if (!existEmail) {
      user.save().then((data) => {
        const tokenData = ({email: req.body.email, password: req.body.password});
        const token = getToken(tokenData);
        res.status(201).json({'response': data, 'token': token});
      }).catch(() => {
        res.status(500).json({'message': 'Internal server error'});
      });
    } else {
      res.status(409).json({'message': 'Account Exists! Unable to create an account with the provided username or email.'});
    }
  });
});

router.get('/getUserById', (req, res) => {
  const userId = req.query.userId;
  TestUser.findById({_id: userId}).then((data) => {
    if (data) {
      return res.status(200).json({'response': data});
    }
    res.status(204).json({'message': `User with ${userId} not found!`});
  }).catch(() => {
    res.status(500).json({'message': 'Internal server error'});
  });

});

router.get('/getJwt', (req, res) => {
  const secretKey = 'SECRET';
  const token = jwt.sign({'body': req.body}, secretKey);
  res.send('Bearer ' + token);
});

module.exports = router;

function getToken(data) {
  const secretKey = 'SECRET';
  const token = jwt.sign({'body': data}, secretKey);
  return 'Bearer ' + token;
}

function isAuthorized(req, res, next) {
  const loginToken = req.headers.authorization;
  if (loginToken) {
    const token = loginToken.split(' ')[1];
    const secretKey = 'SECRET';

    jwt.verify(token, secretKey, (err, decode) => {
      if (err) res.status(401).json({error: 'Unauthorized User'});
      res.status(200).json(decode);
      return next();
    });
  } else {
    res.status(401).json({error: 'Not Authorized'});
  }
}

function emailExistValidation(email) {
  const findUser = TestUser.findOne({email: email});
  return findUser;
}
