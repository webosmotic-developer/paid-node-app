const express = require('express');
const mongo = require('mongoose');
const cors = require('cors');
const TestUser = require('./route/paid');

const app = express();
app.use(cors());
app.use(express.json());
app.use('/user', TestUser);

mongo.connect('mongodb://localhost:27017/TestUserDB', { useNewUrlParser: true, useUnifiedTopology: true});

mongo.connection.on('error', (err) => {
  if (err) console.log('Something is wrong', err);
}).once('open', () => {
  console.log('MongoDB database connection established successfully');
});

const port = 3001;

app.listen(port, function() {
  console.log('Server is running on Port: ' + port);
});
