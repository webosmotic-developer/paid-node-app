const mongo = require('mongoose');

const Schema = mongo.Schema;

let TestUser = new Schema(
  {
    first_name: {
      type: String,
      required: [true, 'First Name is required']
    },
    last_name: {
      type: String,
      required: [true, 'Last Name is required']
    },
    phone: {
      type: Number,
      minLength: [10, 'Number should be 10 digit'],
      maxLength: [10, 'Number should be 10 digit'],
      required: [true, 'Contact is required']
    },
    email: {
      type: String,
      required: [true, 'Email is required']
    },
    password: {
      type: String,
      required: [true, 'Password is required']
    }
  },
  {
    collection: 'TestUser',
    timestamp: true
  }
);

module.exports = mongo.model('TestUser', TestUser);
